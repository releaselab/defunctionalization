A deductive verification framework for higher-order programs, using
defunctionalization.

Soon, this will be integrated with the Cameleer project: https://github.com/mariojppereira/cameleer